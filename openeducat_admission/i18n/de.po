msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: POEditor.com\n"
"Project-Id-Version: openeducat_admission\n"
"Language: de\n"

#. module: openeducat_admission
#: model:ir.model.fields,help:openeducat_admission.field_op_student_fees_details_invoice_state
msgid " * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
" * The 'Pro-forma' status is used when the invoice does not have an invoice number.\n"
" * The 'Open' status is used when user creates invoice, an invoice number is generated. It stays in the open status till the user pays the invoice.\n"
" * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
" * The 'Cancelled' status is used when user cancel invoice."
msgstr " * Der Status 'Entwurf' wird verwendet, wenn ein Benutzer eine neue und nicht bestätigte Rechnung verschlüsselt.\n"
" * Der Pro-forma-Status wird verwendet, wenn die Rechnung keine Rechnungsnummer hat.\n"
" * Der Status 'Öffnen' wird verwendet, wenn der Benutzer eine Rechnung erstellt und eine Rechnungsnummer generiert. Es bleibt geöffnet, bis der Benutzer die Rechnung bezahlt.\n"
" * Der Status \"Bezahlt\" wird automatisch festgelegt, wenn die Rechnung bezahlt wird. Die zugehörigen Journaleinträge können abgeglichen werden oder nicht.\n"
" * Der Status 'Storniert' wird verwendet, wenn der Benutzer die Rechnung storniert."

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "<b>Course : </b>"
msgstr "<b> Kurs: </ b>"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "<b>From Date: </b>"
msgstr "<b> Ab Datum: </ b>"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "<b>To Date:</b>"
msgstr "<b> Bisher: </ b>"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "<strong>Admission Analysis</strong>"
msgstr "<strong> Zulassungsanalyse </ strong>"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "<strong>Total Number of Students :</strong>"
msgstr "<strong> Gesamtzahl der Schüler: </ strong>"

#. module: openeducat_admission
#: model:ir.ui.menu,name:openeducat_admission.menu_op_admission_submenu
msgid "ADMISSIONS"
msgstr "ADMISSIONS"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Address"
msgstr "Adresse"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_op_admission
msgid "Admission"
msgstr "Zulassung"

#. module: openeducat_admission
#: model:ir.actions.act_window,name:openeducat_admission.act_open_op_admission_view_pivot
#: model:ir.actions.report,name:openeducat_admission.action_report_report_admission_analysis
#: model:ir.ui.menu,name:openeducat_admission.menu_admission_analysis
#: model:ir.ui.view,arch_db:openeducat_admission.admission_analysis_form
msgid "Admission Analysis"
msgstr "Zulassungsanalyse"

#. module: openeducat_admission
#: model:ir.actions.act_window,name:openeducat_admission.admission_analysis_act
#: model:ir.ui.menu,name:openeducat_admission.menu_admission_analysis_report
msgid "Admission Analysis Report"
msgstr "Zulassungsanalysebericht"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: selection:op.admission,state:0
msgid "Admission Confirm"
msgstr "Zulassung bestätigen"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_admission_date
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Admission Date"
msgstr "Zulassungsdatum"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Admission Detail"
msgstr "Zulassungsdetail"

#. module: openeducat_admission
#: selection:op.admission.register,state:0
msgid "Admission Process"
msgstr "Zulassungsverfahren"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_op_admission_register
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_id
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_pivot
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_search
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_tree
msgid "Admission Register"
msgstr "Zulassungsregister"

#. module: openeducat_admission
#: model:ir.actions.act_window,name:openeducat_admission.act_open_op_admission_register_view
#: model:ir.ui.menu,name:openeducat_admission.menu_admission_register
msgid "Admission Registers"
msgstr "Zulassungsregister"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_admission_ids
#: model:ir.ui.menu,name:openeducat_admission.menu_op_admission_root
msgid "Admissions"
msgstr "Zulassungen"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Already Student"
msgstr "Schon Student"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_application_date
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Application Date"
msgstr "Antragsdatum"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission.py:181
msgid "Application Date should be between Start Date &                     End Date of Admission Register."
msgstr "Das Bewerbungsdatum sollte zwischen dem Startdatum und dem Enddatum des Zulassungsregisters liegen."

#. module: openeducat_admission
#: selection:op.admission.register,state:0
msgid "Application Gathering"
msgstr "Anwendungserfassung"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "Application No."
msgstr "Antrag Nr."

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_application_number
msgid "Application Number"
msgstr "Anmeldenummer"

#. module: openeducat_admission
#: model:ir.ui.menu,name:openeducat_admission.menu_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Applications"
msgstr "Anwendungen"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_batch_id
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Batch"
msgstr "Semester"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_birth_date
msgid "Birth Date"
msgstr "Geburtsdatum"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission.py:190
msgid "Birth Date can't be greater than current date!"
msgstr "Das Geburtsdatum darf nicht größer sein als das aktuelle Datum!"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.admission_analysis_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Cancel"
msgstr "Stornieren"

#. module: openeducat_admission
#: selection:op.admission,state:0
#: selection:op.admission.register,state:0
msgid "Cancelled"
msgstr "Abgesagt"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_city
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "City"
msgstr "Stadt"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Confirm"
msgstr "Bestätigen"

#. module: openeducat_admission
#: selection:op.admission,state:0
#: selection:op.admission.register,state:0
msgid "Confirmed"
msgstr "Bestätigt"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_country_id
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Country"
msgstr "Land"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_course_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_course_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_course_id
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_search
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Course"
msgstr "Kurs"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_graph
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_pivot
msgid "Course By Admission"
msgstr "Kurs nach Zulassung"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_student_inherit_admission_form
msgid "Create Invoice"
msgstr "Rechnung erstellen"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_create_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_create_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_create_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_create_uid
msgid "Created by"
msgstr "Erstellt von"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_create_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_create_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_create_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_create_date
msgid "Created on"
msgstr "Erstellt am"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_display_name
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_display_name
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_display_name
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_display_name
#: model:ir.model.fields,field_description:openeducat_admission.field_report_openeducat_admission_report_admission_analysis_display_name
msgid "Display Name"
msgstr "Anzeigename"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
#: selection:op.admission,state:0
#: selection:op.admission.register,state:0
msgid "Done"
msgstr "Erledigt"

#. module: openeducat_admission
#: selection:op.admission,state:0
#: selection:op.admission.register,state:0
#: selection:op.student.fees.details,state:0
msgid "Draft"
msgstr "Entwurf"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_due_date
msgid "Due Date"
msgstr "Geburtstermin"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Educational Detail"
msgstr "Bildungsdetail"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_email
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Email"
msgstr "Email"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_end_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_end_date
msgid "End Date"
msgstr "Endtermin"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission_register.py:71
msgid "End Date cannot be set before                 Start Date."
msgstr "Das Enddatum kann nicht vor dem Startdatum festgelegt werden."

#. module: openeducat_admission
#: code:addons/openeducat_admission/wizard/admission_analysis_wizard.py:42
msgid "End Date cannot be set before             Start Date."
msgstr "Das Enddatum kann nicht vor dem Startdatum festgelegt werden."

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Enroll"
msgstr "Einschreiben"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Enrolled"
msgstr "Eingeschrieben"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_family_business
msgid "Family Business"
msgstr "Familienbetrieb"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_family_income
msgid "Family Income"
msgstr "Familieneinkommen"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_fees
msgid "Fees"
msgstr "Gebühren"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_amount
msgid "Fees Amount"
msgstr "Gebührenbetrag"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_detail_ids
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_student_inherit_admission_form
msgid "Fees Collection Details"
msgstr "Gebührenerhebung Details"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_fees_line_id
msgid "Fees Line"
msgstr "Gebühren Linie"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_fees_term_id
msgid "Fees Term"
msgstr "Gebühren Laufzeit"

#. module: openeducat_admission
#: selection:op.admission,gender:0
msgid "Female"
msgstr "Weiblich"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_name
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "First Name"
msgstr "Vorname"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_gender
msgid "Gender"
msgstr "Geschlecht"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_search
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Group By..."
msgstr "Gruppiere nach..."

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_id_8673
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_id
#: model:ir.model.fields,field_description:openeducat_admission.field_report_openeducat_admission_report_admission_analysis_id
msgid "ID"
msgstr "ICH WÜRDE"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_invoice_id
msgid "Invoice"
msgstr "Rechnung"

#. module: openeducat_admission
#: selection:op.student.fees.details,state:0
msgid "Invoice Created"
msgstr "Rechnung erstellt"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_is_student
msgid "Is Already Student"
msgstr "Ist schon Student"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis___last_update
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission___last_update
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register___last_update
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details___last_update
#: model:ir.model.fields,field_description:openeducat_admission.field_report_openeducat_admission_report_admission_analysis___last_update
msgid "Last Modified on"
msgstr "Zuletzt geändert am"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_last_name
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Last Name"
msgstr "Nachname"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_write_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_write_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_write_uid
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_write_uid
msgid "Last Updated by"
msgstr "Zuletzt aktualisiert von"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_write_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_write_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_write_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_write_date
msgid "Last Updated on"
msgstr "Zuletzt aktualisiert am"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "Mail :"
msgstr "Mail:"

#. module: openeducat_admission
#: selection:op.admission,gender:0
msgid "Male"
msgstr "Männlich"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_max_count
msgid "Maximum No. of Admission"
msgstr "Maximale Anzahl der Zulassung"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_middle_name
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Middle Name"
msgstr "Zweiter Vorname"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission_register.py:81
msgid "Min Admission can't be greater than Max Admission"
msgstr "Die Mindestzulassung darf nicht höher sein als die Höchstzulassung"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_min_count
msgid "Minimum No. of Admission"
msgstr "Minimale Anzahl der Zulassung"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_mobile
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Mobile"
msgstr "Handy, Mobiltelefon"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_name
msgid "Name"
msgstr "Name"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_nbr
msgid "No of Admission"
msgstr "Nr. Der Zulassung"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission_register.py:79
msgid "No of Admission should be positive!"
msgstr "Die Zulassungsnummer sollte positiv sein!"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Open Student Profile"
msgstr "Öffnen Sie das Schülerprofil"

#. module: openeducat_admission
#: selection:op.admission,gender:0
msgid "Other"
msgstr "Andere"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_partner_id
msgid "Partner"
msgstr "Partner"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: selection:op.admission,state:0
msgid "Pending"
msgstr "steht aus"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Personal Detail"
msgstr "Persönliches Detail"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_phone
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Phone"
msgstr "Telefon"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "Phone :"
msgstr "Telefon:"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission.py:205
msgid "Please assign batch."
msgstr "Bitte Charge zuordnen."

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_prev_course_id
msgid "Previous Course"
msgstr "Vorheriger Kurs"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_prev_institute_id
msgid "Previous Institute"
msgstr "Vorheriges Institut"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_prev_result
msgid "Previous Result"
msgstr "Vorheriges Ergebnis"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.admission_analysis_form
msgid "Print"
msgstr "Drucken"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_product_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_product_id
msgid "Product"
msgstr "Produkt"

#. module: openeducat_admission
#: model:ir.actions.act_window,name:openeducat_admission.act_open_op_admission_view
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_tree
msgid "Registration"
msgstr "Anmeldung"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Reject"
msgstr "Ablehnen"

#. module: openeducat_admission
#: selection:op.admission,state:0
msgid "Rejected"
msgstr "Abgelehnt"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Set to Draft"
msgstr "Auf Entwurf setzen"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Start Admission"
msgstr "Starten Sie die Zulassung"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_form
msgid "Start Application"
msgstr "Anwendung starten"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_admission_analysis_start_date
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_start_date
msgid "Start Date"
msgstr "Anfangsdatum"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_state
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_invoice_state
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "State"
msgstr "Zustand"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_state_id
msgid "States"
msgstr "Zustände"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_register_state
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_state
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_search
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_search
msgid "Status"
msgstr "Status"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_street
msgid "Street"
msgstr "Straße"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Street..."
msgstr "Straße..."

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_street2
msgid "Street2"
msgstr "Straße Nummer 2"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_student_id
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_student_id
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Student"
msgstr "Schüler"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_op_student_fees_details
msgid "Student Fees Details"
msgstr "Details zu den Studiengebühren"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.report_admission_analysis
msgid "Student Name"
msgstr "Name des Studenten"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Submit"
msgstr "einreichen"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_student_fees_details_date
msgid "Submit Date"
msgstr "Abgabetermin"

#. module: openeducat_admission
#: selection:op.admission,state:0
msgid "Submitted"
msgstr "Eingereicht"

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission.py:368
#: code:addons/openeducat_admission/models/student.py:64
msgid "The value of the deposit amount must be                              positive."
msgstr "Der Wert des Einzahlungsbetrags muss positiv sein."

#. module: openeducat_admission
#: code:addons/openeducat_admission/models/admission.py:363
#: code:addons/openeducat_admission/models/student.py:59
msgid "There is no income account defined for this product: \"%s\".                    You may have to install a chart of account from Accounting                    app, settings menu."
msgstr "Für dieses Produkt ist kein Einkommenskonto definiert: \"% s\". Möglicherweise müssen Sie einen Kontenplan über die Buchhaltungs-App im Einstellungsmenü installieren."

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_title
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "Title"
msgstr "Titel"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_student_inherit_admission_form
msgid "View Invoice"
msgstr "Rechnung anzeigen"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_form
msgid "ZIP"
msgstr "POSTLEITZAHL"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_zip
msgid "Zip"
msgstr "Postleitzahl"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_admission_analysis
msgid "admission.analysis"
msgstr "Aufnahme.Analyse"

#. module: openeducat_admission
#: model:ir.model.fields,field_description:openeducat_admission.field_op_admission_image
msgid "image"
msgstr "Bild"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_op_student
msgid "op.student"
msgstr "op.student"

#. module: openeducat_admission
#: model:ir.ui.view,arch_db:openeducat_admission.view_op_admission_register_search
msgid "product"
msgstr "Produkt"

#. module: openeducat_admission
#: model:ir.model,name:openeducat_admission.model_report_openeducat_admission_report_admission_analysis
msgid "report.openeducat_admission.report_admission_analysis"
msgstr "report.openeducat_admission.report_admission_analysis"

